
const form = document.querySelector('#btn');
const firstName = document.getElementById('name');
const email = document.getElementById('email');
const phoneNumber = document.getElementById('mobile');
const mainSection = document.querySelector('.main-section');
const pricingSection = document.querySelector('.pricing-section');
const asideBar = document.querySelector('.aside-bar');
const pricePlan = document.querySelector('.price-plan');
const addOnSection = document.querySelector('.add-on-section');
const pickPlan = document.querySelectorAll('.pick-plan');
const selectMonthly = document.querySelector('.select-monthly');
const selectAnnually = document.querySelector('.select-annually');
const summarySection = document.querySelector('.summary-section');
const yearBonus = document.querySelectorAll('.year-bonus');

const price = document.querySelectorAll('.price');
const button = document.querySelector(".toggle-selector");
const steps = document.querySelectorAll('.steps');

const priceType = document.querySelectorAll('.price-type');
const finalPaymentType = document.querySelector('#final-payment-type');
const finalPaymentAmount = document.querySelector('#final-payment-amount');

const change = document.querySelector('.change');
const addOnType = document.querySelectorAll('.add-on-type');
const finalPaymentContainer = document.querySelector('.final-payment');
const addOnCheckBox = document.querySelectorAll('.addon-checkbox');

const selectPlanOne = document.querySelector('.selected-plan-1');
const finalTotalAmount = document.querySelector('.final-total-amount');
const totalPaymentType = document.querySelector('.total-payment-type');
const confirm = document.querySelector('#confirm');
const thankYouSection = document.querySelector('.thank-you-section');
const priceError = document.querySelector('.price-error');

let isMonthly = true;
let count = 1;
let totalSum = 0;

let selectedPlan = JSON.parse(localStorage.getItem('selectedPlan')) || null;
const selectedItems = [];

const monthly = ['$9/mo', '$12/mo', '$15/mo'];
const year = ['$90/yr', '$120/yr', '$150/yr'];

const monthlyAddon = ['+$1/mo', '+$2/mo', '+$2/mo'];
const yearAddon = ['+$10/yr', '+$20/yr', '+$20/yr'];




window.addEventListener('beforeunload', function (e) {
    if (count === 5) {
        localStorage.clear();
        return;
    }
    e.preventDefault();
    e.returnValue = 'Are you sure you want to leave this page?';
});




count = parseInt(localStorage.getItem('currentStep')) || 1;

// It will shows the current step
showStep(count);

if (count == 2) {
    mainSection.style.display = 'none';
    pricingSection.style.display = 'flex';

}

if (count == 3) {
    mainSection.style.display = 'none';
    pricingSection.style.display = 'none';
    addOnSection.style.display = 'flex';

}

if (count == 4) {
    mainSection.style.display = 'none';
    pricingSection.style.display = 'none';
    addOnSection.style.display = 'none';
    summarySection.style.display = 'flex';
    // selectedAddOnTypeAndPrice();
}

if (count == 5) {
    mainSection.style.display = 'none';
    pricingSection.style.display = 'none';
    addOnSection.style.display = 'none';
    summarySection.style.display = 'none';
    thankYouSection.style.display = 'flex';
}


function errorOccurred(element, text) {
    const errorSpan = element.parentNode.querySelector('.error-message');
    errorSpan.textContent = text;
    element.style.border = '1px solid red';
}

function borderViolet(element) {
    const errorSpan = element.parentNode.querySelector('.error-message');
    errorSpan.textContent = '';
    element.style.border = '1px solid #7771AF';
}


function checkName(name) {
    const checkCharacters = ['~', '!', '@', '#', '$', '%', '*', '(', ')', '+', '=', ':', '^', '[', ']', '_', '-', ';', '>', '<', '/', '?', '|', '\\', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    for (const char of checkCharacters) {
        if (name.includes(char)) {
            return true;
        }
    }
    return false;
}

function emailSpaces(emailValue) {
    if (emailValue.match(/\s/g)) {
        return true;
    }
    return false;

}

function emailSymbol(emailValue) {
    if (emailValue.indexOf('@') === -1) {
        return true;
    }
    return false;

}

function emailLocalAndDomainPartEmpty(emailValue) {
    const [localPart, domainPart] = emailValue.split('@');
    if (localPart.length === 0 || domainPart.length === 0) {
        return true;
    } else {
        return false;
    }

}

function emailLocalAndDomain(emailValue) {
    const [localPart, domainPart] = emailValue.split('@');
    const domainParts = domainPart.split('.');

    if (domainParts.length < 2 || domainParts.some(part => part.length === 0)) {
        return true;
    } else {
        return false;
    }

}


function nameValidation(firstName) {
    let checkList = false;
    if (firstName.value === '' || firstName.value.trim() === '') {
        errorOccurred(firstName, `This field is required`);
        checkList = true;
    } else if (checkName(firstName.value)) {
        errorOccurred(firstName, ` special characters,numbers not allowed`);
        checkList = true;

    } else if (firstName.value.length < 3) {
        errorOccurred(firstName, 'Please enter atleast 3 letters');
        checkList = true;
    } else {
        borderViolet(firstName);
    }

    return checkList;

}


function emailValidation(email) {
    let checkList = false;

    if (email.value === '') {
        errorOccurred(email, 'This field is required');
        checkList = true;
    } else if (emailSymbol(email.value)) {
        errorOccurred(email, `Email includes @ symbol`);
        checkList = true;
    } else if (emailSpaces(email.value)) {
        errorOccurred(email, 'Spaces are not acceptable');
        checkList = true;
    } else if (emailLocalAndDomainPartEmpty(email.value)) {
        errorOccurred(email, `local and domain part length should not be 0`);
        checkList = true;
    } else if (emailLocalAndDomain(email.value)) {
        errorOccurred(email, `domain part length should not zero and less than 2`);
        checkList = true;
    } else {
        borderViolet(email);
    }

    return checkList;

}

function phoneNumberValidation(phoneNumber) {
    let checkList = false;

    const phoneNumberValue = phoneNumber.value;
    const phoneNumberWithoutSpaces = phoneNumberValue.replace(/\s/g, '');
    let numericPart = phoneNumberWithoutSpaces.substring(1);

    if (phoneNumberWithoutSpaces === '') {
        errorOccurred(phoneNumber, `This field is required`);
        checkList = true;
    } else if (!(phoneNumberWithoutSpaces.startsWith('+'))) {
        errorOccurred(phoneNumber, `Phone number should contain only digits after "+"`);
        checkList = true;
    } else if (!(numericPart.length >= 11 && numericPart.length <= 18)) {
        errorOccurred(phoneNumber, `Phone number length should be greater than or equal to 10`);
        checkList = true;
    } else if (!(/^\d+$/.test(numericPart))) {
        errorOccurred(phoneNumber, `Enter only numbers`);
        checkList = true;
    } else {
        borderViolet(phoneNumber);
    }

    return checkList;
}



//  form validation click event

form.addEventListener('click', (event) => {
    event.preventDefault();
    console.log("clicked");

    let checkList = false;

    checkList = nameValidation(firstName);

    checkList = emailValidation(email);

    checkList = phoneNumberValidation(phoneNumber);

    if (!checkList) {
        count++;
        mainSection.style.display = 'none';
        pricingSection.style.display = 'flex';

        localStorage.setItem('currentStep', JSON.stringify(count));

        let userValues = {
            firstName: firstName.value,
            email: email.value,
            phoneNumber: phoneNumber.value
        }

        localStorage.setItem('users', JSON.stringify(userValues));
        showStep(count);

    }
});


firstName.addEventListener('keyup', () => {
    nameValidation(firstName);
});

email.addEventListener('keyup', () => {
    emailValidation(email);
});

phoneNumber.addEventListener('keyup', () => {
    phoneNumberValidation(phoneNumber);
})


//  Show steps functionality

function stepsChange(step) {
    steps.forEach((step, index) => {
        step.style.backgroundColor = '#483DFF';
        step.style.color = 'white';
    });
}

function showStep(step) {
    if (step === 1) {
        mainSection.style.display = 'flex';
        pricingSection.style.display = 'none';
        addOnSection.style.display = 'none';
        summarySection.style.display = 'none';
        thankYouSection.style.display = 'none';
        stepsChange(step)
        steps[0].style.backgroundColor = '#BDE1FD';
        steps[0].style.color = 'black';
    } else if (step === 2) {
        mainSection.style.display = 'none';
        pricingSection.style.display = 'flex';
        addOnSection.style.display = 'none';
        summarySection.style.display = 'none';
        thankYouSection.style.display = 'none'

        stepsChange(step);
        steps[1].style.backgroundColor = '#BDE1FD';
        steps[1].style.color = 'black';
    } else if (step === 3) {
        mainSection.style.display = 'none';
        pricingSection.style.display = 'none';
        addOnSection.style.display = 'flex';
        summarySection.style.display = 'none';
        thankYouSection.style.display = 'none';

        stepsChange(step);
        steps[2].style.backgroundColor = '#BDE1FD';
        steps[2].style.color = 'black';

    } else if (step === 4) {
        mainSection.style.display = 'none';
        pricingSection.style.display = 'none';
        addOnSection.style.display = 'none';
        summarySection.style.display = 'flex';
        thankYouSection.style.display = 'none';

        stepsChange(step);
        steps[3].style.backgroundColor = '#BDE1FD';
        steps[3].style.color = 'black';

    } else if (step === 5) {
        mainSection.style.display = 'none';
        pricingSection.style.display = 'none';
        addOnSection.style.display = 'none';
        summarySection.style.display = 'none';
        thankYouSection.style.display = 'flex';

    }
}


// toggle plan  step-2

button.addEventListener("click", togglePaymentPlan);

function togglePaymentPlan() {
    if (isMonthly) {

        price.forEach((item, index) => {
            item.textContent = `${year[index]}`;
            item.nextSibling.textContent = `2 months free`;
        });

        button.style.justifyContent = 'flex-end';
        selectMonthly.style.color = 'gray';
        selectAnnually.style.color = '#022453';

    } else {
        price.forEach((item, index) => {
            item.textContent = `${monthly[index]}`;
            item.nextSibling.textContent = '';
        });

        button.style.justifyContent = 'flex-start';
        selectAnnually.style.color = 'gray'
        selectMonthly.style.color = '#022453';
    }

    isMonthly = !isMonthly

    updatePickPlanContent(isMonthly);
    recalculateTotalSum();
}

togglePaymentPlan();



const priceCards = document.querySelectorAll('.price-card-container');
const nextStepButton = document.querySelectorAll('.price-btn');


priceCards.forEach((card, index) => {
    if (selectedPlan !== null && selectedPlan === index) {
        card.classList.add('selected');
    } else {
        card.classList.remove('selected');
    }

    card.addEventListener('click', () => {
        priceCards.forEach((item) => item.classList.remove('selected'));
        card.classList.add('selected');
        selectedPlan = index;
        localStorage.setItem('selectedPlan', JSON.stringify(selectedPlan));

        priceError.style.display = 'none';
    });
});


// Next step click event handler functionality

nextStepButton.forEach((nextStep) => {
    nextStep.addEventListener('click', handleNextStep)
})



function handleNextStep() {

    if (selectedPlan !== null) {
        console.log(`Selected plan: ${selectedPlan}`);
        if (count === 2) {
            count++;
            localStorage.setItem('currentStep', JSON.stringify(count));
            showStep(count);
            finalPayment();


        } else if (count === 3) {
            count++;
            localStorage.setItem('currentStep', JSON.stringify(count));
            showStep(count);
            finalPayment();

        } else if (count === 4) {
            count++;
            localStorage.setItem('currentStep', JSON.stringify(count));
            showStep(count);
        }

        priceError.textContent = '';

    } else {
        if (count === 2) {
            priceError.style.display = 'block';
            priceError.textContent = '*please select a plan';
        }

    }

    updateTotalAmount();
}



// toggle button and pick plan

function updatePickPlanContent(isMonthly) {
    const addonPrices = isMonthly ? monthlyAddon : yearAddon;
    pickPlan.forEach((plan, index) => {
        plan.textContent = addonPrices[index];
    });
}

// Calling the function to set the initial content based on the default monthly plan

updatePickPlanContent(isMonthly);


//  Go back button functionality 

const goBack = document.querySelectorAll('.go-back');


goBack.forEach((item, index) => {
    item.addEventListener('click', goBackPreviousStep)
})


function goBackPreviousStep() {

    if (count == 2) {
        mainSection.style.display = 'flex';
        pricingSection.style.display = 'none';
        addOnSection.style.display = 'none';
        summarySection.style.display = 'none';
        thankYouSection.style.display = 'none';
        count--;
        // console.log("inside");
        localStorage.setItem('currentStep', JSON.stringify(count));
        const userValues = JSON.parse(localStorage.getItem('users'));

        firstName.value = userValues.firstName;
        email.value = userValues.email;
        phoneNumber.value = userValues.phoneNumber;

        showStep(count);
    }

    if (count == 3) {
        pricingSection.style.display = 'flex';
        mainSection.style.display = 'flex';
        addOnSection.style.display = 'none';
        summarySection.style.display = 'none';
        thankYouSection.style.display = 'none';
        count--;
        localStorage.setItem('currentStep', JSON.stringify(count));
        showStep(count);
    }

    if (count == 4) {
        addOnSection.style.display = 'flex';
        mainSection.style.display = 'flex';
        pricingSection.style.display = 'none';
        summarySection.style.display = 'none';
        thankYouSection.style.display = 'none';
        count--;
        localStorage.setItem('currentStep', JSON.stringify(count));
        showStep(count);
    }
}

//  Add on step-3

const addonCheckboxes = document.querySelectorAll('.addon-checkbox');
const addOnCards = document.querySelectorAll('.add-on');


// Step-3 Add-on Selection

function clearSelectedAddOns() {
    const selectedAddOns = document.querySelectorAll('.selected-plan');
    selectedAddOns.forEach((addOn) => {
        finalPaymentContainer.removeChild(addOn);
    });
}


function removeDeselectedAddOn(index) {
    const indexOfDeselected = selectedItems.indexOf(index);

    if (indexOfDeselected !== -1) {
        selectedItems.splice(indexOfDeselected, 1);

        const selectedAddOns = document.querySelectorAll('.selected-plan');

        if (selectedAddOns[indexOfDeselected]) {

            const newAdd = selectedAddOns[indexOfDeselected];

            const amountValue = newAdd.querySelector('.amount').textContent;

            totalSum = totalSum - parseInt(amountValue.replace('$', ''));

            console.log(totalSum, "total-sum-remove");
            console.log(newAdd, "new");
            finalPaymentContainer.removeChild(selectedAddOns[indexOfDeselected]);
            // addOnAmount
        }
    }
}


const addOnSelectItems = JSON.parse(localStorage.getItem('addOnItems')) || [];

addonCheckboxes.forEach((checkbox, index) => {
    const isSelected = addOnSelectItems.includes(index);

    checkbox.checked = isSelected;

    if (isSelected) {
        addOnCards[index].classList.add('selected');
        addOnCards[index].style.border = '1px solid #483FFF';
    } else {
        addOnCards[index].classList.remove('selected');
        addOnCards[index].style.border = '1px solid #ccc';
    }

    checkbox.addEventListener('change', () => {
        if (checkbox.checked) {
            clearSelectedAddOns();
            addOnCards[index].classList.add('selected');
            addOnCards[index].style.border = '1px solid #483FFF';
            addOnSelectItems.push(index);

            selectedItems.push(index);

            localStorage.setItem('addOnItems', JSON.stringify(addOnSelectItems));
            console.log(addOnSelectItems, "add");
            selectedAddOnTypeAndPrice();

            console.log(selectedItems, "inside-items");

            const sumValue = parseInt(addOnCards[index].querySelector('.pick-plan').textContent.replace('$', ''));

            console.log(addOnCards[index].querySelector('.pick-plan').textContent, "picking");

            totalSum += sumValue;

            console.log(totalSum, "total-sum");

            
        } else {
            removeDeselectedAddOn(index);

            addOnCards[index].classList.remove('selected');
            addOnCards[index].style.border = '1px solid #ccc';

            const indexOfUnchecked = addOnSelectItems.indexOf(index);

            const checkIndex = selectedItems.indexOf(index);

            if (indexOfUnchecked !== -1) {
                addOnSelectItems.splice(indexOfUnchecked, 1);

            }

            if (checkIndex !== -1) {
                selectedItems.splice(checkIndex, 1);
            }
            localStorage.setItem('addOnItems', JSON.stringify(addOnSelectItems));
        }

        updateTotalAmount();
        console.log(selectedItems, "outiside-items");
        recalculateTotalSum();
        // console.log(addOnSelectItems,"outside");
    });
});



//  Step-4 Summary

function finalPayment() {
    priceType.forEach((priceName, index) => {
        if (selectedPlan === index) {
            if (isMonthly) {
                finalPaymentType.textContent = `${priceName.textContent} (Monthly)`;
            } else {
                finalPaymentType.textContent = `${priceName.textContent} (Yearly)`;
            }
        }
    });

    if (selectedPlan !== null) {
        const selectedPrice = isMonthly ? monthly[selectedPlan] : year[selectedPlan];
        finalPaymentAmount.textContent = `${selectedPrice}`;
    }
}

finalPayment();


//  Change the plan functionality

change.addEventListener('click', changePayment);

function changePayment() {
    // togglePaymentPlan();
    // selectedAddOnTypeAndPrice();
    pricingSection.style.display = 'flex';
    summarySection.style.display = 'none';
    count = 2;
    showStep(count);

    recalculateTotalSum();

    localStorage.setItem('currentStep', JSON.stringify(count));

}



function selectedAddOnTypeAndPrice() {

    selectedItems.forEach((item, index) => {
        const planDivContainer = document.createElement('div');
        const typeValue = addOnType[item].textContent;
        let pickAmount;

        if (isMonthly) {
            pickAmount = monthlyAddon[item];
        } else {
            pickAmount = yearAddon[item];
        }

        console.log(pickAmount, "pick-amount")

        planDivContainer.classList.add('selected-plan');

        const changePaymentDiv = document.createElement('div');
        changePaymentDiv.classList.add('change-payment');

        const typeOfAddOn = document.createElement('p');
        typeOfAddOn.classList.add('finish-check');
        typeOfAddOn.textContent = typeValue;

        changePaymentDiv.appendChild(typeOfAddOn);

        const spanAmount = document.createElement('span');
        spanAmount.classList.add('amount');
        spanAmount.textContent = pickAmount;


        planDivContainer.append(changePaymentDiv, spanAmount);

        finalPaymentContainer.appendChild(planDivContainer);
    });

    updateTotalAmount();

}


// selectedAddOnTypeAndPrice();



totalSum += parseInt(finalPaymentAmount.textContent.replace('$', ''));

function updateTotalAmount() {
    if (isMonthly) {
        finalTotalAmount.textContent = `+${totalSum}/mo`;
        totalPaymentType.textContent = `(per month)`;
    } else {
        finalTotalAmount.textContent = `+${totalSum}/yr`;
        totalPaymentType.textContent = `(per year)`;
    }
}


function recalculateTotalSum() {
    totalSum = 0;

    if (selectedPlan !== null) {
        const selectedPrice = isMonthly ? monthly[selectedPlan] : year[selectedPlan];
        totalSum += parseInt(selectedPrice.replace('$', ''));
    }

    selectedItems.forEach((item) => {
        const pickAmount = pickPlan[item].textContent;
        totalSum += parseInt(pickAmount.replace('$', ''));
    });

    updateTotalAmount();
}


// 

selectMonthly.addEventListener('click', () => {
    if (!isMonthly) {
        togglePaymentPlan();
        // selectedAddOnTypeAndPrice();
    }
});

selectAnnually.addEventListener('click', () => {
    if (isMonthly) {
        togglePaymentPlan();
        // selectedAddOnTypeAndPrice();
    }
});



